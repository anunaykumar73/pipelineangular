import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

  constructor() {
    localStorage.setItem('employees', JSON.stringify(
      [
        {"firstName": "Test1", "lastName": "User1", "email": "test.user1@gmail.com", "passwords": {
          "password":"ITT@123", "confirmPassword": "ITT@123456"
        }},
        {"firstName": "Test2", "lastName": "User2", "email": "test.user2@gmail.com", "passwords": {
          "password":"ITT@123", "confirmPassword": "ITT@123456"
        }},
        {"firstName": "Test3", "lastName": "User3", "email": "test.user3@gmail.com", "passwords": {
          "password":"ITT@123", "confirmPassword": "ITT@123456"
        }},
        {"firstName": "Test4", "lastName": "User4", "email": "test.user4@gmail.com", "passwords": {
          "password":"ITT@123", "confirmPassword": "ITT@123456"
        }},
        {"firstName": "Test5", "lastName": "User5", "email": "test.user5@gmail.com", "passwords": {
          "password":"ITT@123", "confirmPassword": "ITT@123456"
        }}
      ]
    ));
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('inside Fake backend intercept method');
    console.log(request);
    // array in local storage for registered employees
    let employees: any[] = JSON.parse(localStorage.getItem('employees')) || [];

    // wrap in delayed observable to simulate server api call
    return Observable.of(null).mergeMap(() => {

      // get employees
      if (request.url.endsWith('/api/employees') && request.method === 'GET') {
        return Observable.of(new HttpResponse({ status: 200, body: employees }));
      }

      // pass through any requests not handled above
      return next.handle(request);

    })

    // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
      .materialize()
      .delay(500)
      .dematerialize();
  }
}
