import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {
  }

  getActionWithParam<T>(param: any, path: string, requireHeader: boolean = false) {
    let headers = new HttpHeaders();
    headers = this.setAutorizationHeader(headers, requireHeader);
    const options = {
        headers: headers,
        search: param
    };
    return this.http.get(path, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getAction<T>(path: string, requireHeader: boolean = false) {
    let headers = new HttpHeaders();
    headers = this.setAutorizationHeader(headers, requireHeader);
    const options = {
        headers: headers
    };
    return this.http.get(path, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  postAction<T>(body: T, path: string, requireHeader: boolean = false) {
    let headers = new HttpHeaders({ 'Content-Type': "application/json" });
    headers = this.setAutorizationHeader(headers, requireHeader);
    const options = {
        headers: headers,
    };
    return this.http.post(path, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  postActionForLogin<T>(body: T, path: string) {
    let headers = new HttpHeaders({ 'Content-Type': "application/x-www-form-urlencoded" });
    const options = {
        headers: headers,
    };
    return this.http.post(path, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteAction<T>(param: any, path: string, requireHeader: boolean = false) {
    let headers = new HttpHeaders();
    headers = this.setAutorizationHeader(headers, requireHeader);
    const options = {
        headers: headers,
        search: param
    };
    return this.http.delete(path, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  putAction<T>(param: T, path: string, requireHeader: boolean = false) {
    const body = JSON.stringify(param);
    let headers = new HttpHeaders({ 'Content-Type': "application/json" });
    headers = this.setAutorizationHeader(headers, requireHeader);
    const options = {
        headers: headers
    };
    return this.http.put(path, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private setAutorizationHeader(headers: any, requireHeader: boolean) {
    const token = sessionStorage.getItem('token');
    if (requireHeader) {
      headers.append("Authorization", "bearer " + token);
    }
    return headers;
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }

  private extractData(res: any) {
    if (res.status === 200) {
      return res.json() || {};
    }
    else
      return res || {};
  }
}
