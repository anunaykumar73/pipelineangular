import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Employee } from '../models/employee.model';
import { AppService } from './app.service';
import { AppSettings } from '../app.settings';

@Injectable()
export class EmployeeService {
  employeeList: Employee[] = [];
  constructor(private appService: AppService) { }

  getEmployees(): Observable<any> {
    return this.appService.getAction(AppSettings.API_ENDPOINT + 'employees');
  }

  saveEmployee(emplooyee: any): Observable<any> {
    return this.appService.postAction(emplooyee, AppSettings.API_ENDPOINT + 'employees');
  }

  deleteEmployee(email : string){
    return this.appService.deleteAction(email, AppSettings.API_ENDPOINT + 'employees');
  }

  updateEmployee(employee : Employee){
    return this.appService.putAction(employee, AppSettings.API_ENDPOINT + 'employees', true);
  }
}
